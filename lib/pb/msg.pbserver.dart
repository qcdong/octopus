///
//  Generated code. Do not modify.
//  source: pb/msg.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'package:protobuf/protobuf.dart' as $pb;

import 'dart:core' as $core;
import 'msg.pb.dart' as $2;
import 'comm.pb.dart' as $0;
import 'msg.pbjson.dart';

export 'msg.pb.dart';

abstract class S2CServiceBase extends $pb.GeneratedService {
  $async.Future<$0.Empty> login($pb.ServerContext ctx, $2.OnLogin request);
  $async.Future<$0.Empty> kick($pb.ServerContext ctx, $2.KickReq request);
  $async.Future<$0.Empty> onMsg($pb.ServerContext ctx, $2.Msg request);
  $async.Future<$0.Empty> online($pb.ServerContext ctx, $2.OnlineReq request);
  $async.Future<$0.Empty> offline($pb.ServerContext ctx, $2.OfflineReq request);
  $async.Future<$0.Empty> status($pb.ServerContext ctx, $2.StatusReq request);
  $async.Future<$0.Empty> onUpload($pb.ServerContext ctx, $2.OnUploadReq request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'Login': return $2.OnLogin();
      case 'Kick': return $2.KickReq();
      case 'OnMsg': return $2.Msg();
      case 'Online': return $2.OnlineReq();
      case 'Offline': return $2.OfflineReq();
      case 'Status': return $2.StatusReq();
      case 'OnUpload': return $2.OnUploadReq();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'Login': return this.login(ctx, request as $2.OnLogin);
      case 'Kick': return this.kick(ctx, request as $2.KickReq);
      case 'OnMsg': return this.onMsg(ctx, request as $2.Msg);
      case 'Online': return this.online(ctx, request as $2.OnlineReq);
      case 'Offline': return this.offline(ctx, request as $2.OfflineReq);
      case 'Status': return this.status(ctx, request as $2.StatusReq);
      case 'OnUpload': return this.onUpload(ctx, request as $2.OnUploadReq);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => S2CServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => S2CServiceBase$messageJson;
}

abstract class ChatServiceBase extends $pb.GeneratedService {
  $async.Future<$0.Empty> ping($pb.ServerContext ctx, $0.Empty request);
  $async.Future<$2.Msg> send($pb.ServerContext ctx, $2.Msg request);
  $async.Future<$0.Empty> modifyName($pb.ServerContext ctx, $2.ModifyNameReq request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'Ping': return $0.Empty();
      case 'Send': return $2.Msg();
      case 'ModifyName': return $2.ModifyNameReq();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'Ping': return this.ping(ctx, request as $0.Empty);
      case 'Send': return this.send(ctx, request as $2.Msg);
      case 'ModifyName': return this.modifyName(ctx, request as $2.ModifyNameReq);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => ChatServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => ChatServiceBase$messageJson;
}

