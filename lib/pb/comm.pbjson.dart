///
//  Generated code. Do not modify.
//  source: pb/comm.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = const {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode('CgVFbXB0eQ==');
@$core.Deprecated('Use s2CDataDescriptor instead')
const S2CData$json = const {
  '1': 'S2CData',
  '2': const [
    const {'1': 'Method', '3': 1, '4': 1, '5': 9, '10': 'Method'},
    const {'1': 'Body', '3': 2, '4': 1, '5': 12, '10': 'Body'},
    const {'1': 'Callback', '3': 3, '4': 1, '5': 3, '10': 'Callback'},
    const {'1': 'Error', '3': 4, '4': 1, '5': 9, '10': 'Error'},
  ],
};

/// Descriptor for `S2CData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List s2CDataDescriptor = $convert.base64Decode('CgdTMkNEYXRhEhYKBk1ldGhvZBgBIAEoCVIGTWV0aG9kEhIKBEJvZHkYAiABKAxSBEJvZHkSGgoIQ2FsbGJhY2sYAyABKANSCENhbGxiYWNrEhQKBUVycm9yGAQgASgJUgVFcnJvcg==');
@$core.Deprecated('Use c2SDataDescriptor instead')
const C2SData$json = const {
  '1': 'C2SData',
  '2': const [
    const {'1': 'Method', '3': 1, '4': 1, '5': 9, '10': 'Method'},
    const {'1': 'Body', '3': 2, '4': 1, '5': 12, '10': 'Body'},
    const {'1': 'Callback', '3': 3, '4': 1, '5': 3, '10': 'Callback'},
  ],
};

/// Descriptor for `C2SData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List c2SDataDescriptor = $convert.base64Decode('CgdDMlNEYXRhEhYKBk1ldGhvZBgBIAEoCVIGTWV0aG9kEhIKBEJvZHkYAiABKAxSBEJvZHkSGgoIQ2FsbGJhY2sYAyABKANSCENhbGxiYWNr');
