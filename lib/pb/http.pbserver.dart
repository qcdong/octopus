///
//  Generated code. Do not modify.
//  source: pb/http.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'package:protobuf/protobuf.dart' as $pb;

import 'dart:core' as $core;
import 'http.pb.dart' as $1;
import 'comm.pb.dart' as $0;
import 'http.pbjson.dart';

export 'http.pb.dart';

abstract class PublicServiceBase extends $pb.GeneratedService {
  $async.Future<$0.Empty> regist($pb.ServerContext ctx, $1.RegistReq request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'Regist': return $1.RegistReq();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'Regist': return this.regist(ctx, request as $1.RegistReq);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => PublicServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => PublicServiceBase$messageJson;
}

