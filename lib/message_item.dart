import 'package:bubble/bubble.dart';
import 'package:desktop_context_menu/desktop_context_menu.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:octopus/event/event_widget.dart';
import 'package:octopus/message_item_file.dart';
import 'package:octopus/native.dart';
import 'package:octopus/pb/msg.pb.dart';
import 'package:octopus/wx_expression.dart';

import 'avatar.dart';
import 'data.dart';

class MessageItem extends StatefulWidget {
  MessageItem({
    Key? key,
    required this.msg,
  }) : super(key: key);

  Message msg;

  @override
  State<MessageItem> createState() => _MessageItemState();
}

class _MessageItemState extends State<MessageItem> {
  static const textStyle = TextStyle(
    fontSize: 16,
    decoration: TextDecoration.none,
    fontWeight: FontWeight.normal,
  );

  static const selfBubbleColor = Color.fromARGB(255, 183, 232, 250);
  static const otherBubbleColor = Color.fromARGB(255, 188, 250, 236);

  Widget _createFile() {
    return FileMessageItem(
      msg: widget.msg,
    );
  }

  Widget _createImage() {
    if (widget.msg.status != Status.Done) {
      return const SizedBox(width: 100, height: 100, child: Text("发送中"));
    }

    bool shouldReact = false;
    var url = "http://${Data.server}/downFile?file=${widget.msg.url}";
    return Container(
      constraints: const BoxConstraints(maxHeight: 500, maxWidth: 500),
      child: Listener(
          child: Image(
            image: NetworkImage(url),
          ),
          onPointerDown: (e) {
            shouldReact = e.kind == PointerDeviceKind.mouse &&
                e.buttons == kSecondaryMouseButton;
          },
          onPointerUp: (PointerUpEvent e) async {
            if (shouldReact) {
              var menu = await showContextMenu(menuItems: [
                ContextMenuItem(
                  title: '保存并查看',
                  onTap: () {
                    NativeUtil.downloadAndOpen(widget.msg);
                  },
                ),
                ContextMenuItem(
                  title: '另存为',
                  onTap: () {
                    NativeUtil.saveFileAs(widget.msg);
                  },
                ),
              ]);
              menu?.onTap?.call();
            } else {
              NativeUtil.downloadAndOpen(widget.msg);
              // launchUrl(Uri.parse(url));
            }

            shouldReact = false;
          }),
    );
  }

  Widget _createLeft(BuildContext ctx, {required Widget child}) {
    var theme = Theme.of(ctx);

    var bgcolor = otherBubbleColor;
    if (theme.brightness == Brightness.dark) {
      bgcolor = Color.fromARGB(255, 34, 34, 34);
    }

    var bb = Bubble(
      margin: const BubbleEdges.only(top: 10),
      alignment: Alignment.topLeft,
      nip: BubbleNip.leftTop,
      color: bgcolor,
      child: child,
    );
    if (Data.data.chatTarget.group) {
      var sender = Data.data.getUser(widget.msg.sender);
      return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Avatar(user: sender),
        Expanded(child: bb),
      ]);
    } else {
      return bb;
    }
  }

  Widget _createRight(BuildContext ctx, {required Widget child}) {
    var theme = Theme.of(ctx);

    var bgcolor = selfBubbleColor;
    if (theme.brightness == Brightness.dark) {
      bgcolor = Color.fromARGB(255, 34, 34, 34);
    }

    var bb = Bubble(
      margin: const BubbleEdges.only(
        top: 10,
      ),
      alignment: Alignment.topRight,
      nip: BubbleNip.rightTop,
      color: bgcolor,
      child: child,
    );

    if (Data.data.chatTarget.group) {
      var sender = Data.data.getUser(widget.msg.sender);

      return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Expanded(child: SizedBox()),
        Expanded(child: bb),
        Avatar(user: sender),
      ]);
    } else {
      return bb;
    }
  }

  Widget _createFocus(BuildContext ctx) {
    Text t;
    if (widget.msg.sender == Data.data.me.iD) {
      t = const Text("你戳了他一下");
    } else {
      t = const Text("他戳了你一下");
    }

    var theme = Theme.of(ctx);

    var bgcolor = const Color.fromARGB(255, 219, 219, 219);
    if (theme.brightness == Brightness.dark) {
      bgcolor = Color.fromARGB(255, 34, 34, 34);
    }

    return Container(
        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const Expanded(child: SizedBox()),
          Container(
            padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),
            decoration: BoxDecoration(
              color: bgcolor,
              borderRadius: BorderRadius.circular(50),
            ),
            child: t,
          ),
          const Expanded(child: SizedBox()),
        ]));
  }

  Widget _createWidget(BuildContext ctx) {
    late Widget child;
    if (widget.msg.type == MsgType.FileMsgType) {
      child = _createFile();
    } else if (widget.msg.type == MsgType.ImageMsgType) {
      child = _createImage();
    } else if (widget.msg.type == MsgType.FocusMsgType) {
      return _createFocus(ctx);
    } else {
      child = ExpressionText(widget.msg.content, textStyle);
    }
    if (widget.msg.from == Data.data.me.iD ||
        widget.msg.sender == Data.data.me.iD) {
      return _createRight(ctx, child: child);
    } else {
      return _createLeft(ctx, child: child);
    }
  }

  @override
  Widget build(BuildContext context) {
    return EventWidget(
        buidler: (ctx) {
          return _createWidget(context);
        },
        event: widget.msg);
  }
}
