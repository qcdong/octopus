import 'package:flutter/material.dart';

class LineInput extends StatefulWidget {
  String _hint = '';
  IconData? _icon;
  TextInputType _inputType = TextInputType.text;

  TextEditingController _controller = TextEditingController();

  LineInput({
    Key? key,
    required String hint,
    required IconData icon,
    TextInputType? inputType,
    TextEditingController? controller,
  }) : super(key: key) {
    _hint = hint;
    _icon = icon;
    if (inputType != null) _inputType = inputType;
    if (controller != null) _controller = controller;
  }

  get text => _controller.text;

  @override
  State<StatefulWidget> createState() {
    return _LineInput();
  }
}

class _LineInput extends State<LineInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          TextField(
            keyboardType: widget._inputType,
            obscureText: false,
            controller: widget._controller,
            decoration: InputDecoration(
              hintText: widget._hint,
              prefixIcon: Icon(
                widget._icon,
              ),
              border: InputBorder.none,
              suffixIcon: GestureDetector(
                child: Offstage(
                  child: const Icon(Icons.clear),
                  offstage: widget._controller.text == "",
                ),
                onTap: () {
                  setState(() {
                    widget._controller.clear();
                  });
                },
              ),
            ),
            onChanged: (value) {
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
