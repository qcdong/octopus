///
//  Generated code. Do not modify.
//  source: pb/http.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
import 'comm.pbjson.dart' as $0;

@$core.Deprecated('Use registReqDescriptor instead')
const RegistReq$json = const {
  '1': 'RegistReq',
  '2': const [
    const {'1': 'Nickname', '3': 1, '4': 1, '5': 9, '10': 'Nickname'},
    const {'1': 'Password', '3': 2, '4': 1, '5': 9, '10': 'Password'},
    const {'1': 'Avatar', '3': 3, '4': 1, '5': 9, '10': 'Avatar'},
  ],
};

/// Descriptor for `RegistReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registReqDescriptor = $convert.base64Decode('CglSZWdpc3RSZXESGgoITmlja25hbWUYASABKAlSCE5pY2tuYW1lEhoKCFBhc3N3b3JkGAIgASgJUghQYXNzd29yZBIWCgZBdmF0YXIYAyABKAlSBkF2YXRhcg==');
const $core.Map<$core.String, $core.dynamic> PublicServiceBase$json = const {
  '1': 'Public',
  '2': const [
    const {'1': 'Regist', '2': '.RegistReq', '3': '.Empty'},
  ],
};

@$core.Deprecated('Use publicServiceDescriptor instead')
const $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> PublicServiceBase$messageJson = const {
  '.RegistReq': RegistReq$json,
  '.Empty': $0.Empty$json,
};

/// Descriptor for `Public`. Decode as a `google.protobuf.ServiceDescriptorProto`.
final $typed_data.Uint8List publicServiceDescriptor = $convert.base64Decode('CgZQdWJsaWMSHAoGUmVnaXN0EgouUmVnaXN0UmVxGgYuRW1wdHk=');
