package main

import (
	"Octopus/pb"
	"errors"
	"time"

	"github.com/gorilla/websocket"
	"github.com/qcdong2016/logs"
	"google.golang.org/protobuf/proto"
)

type Msg struct {
	Bytes []byte
	Type  int
}

type WsConn struct {
	ws       *websocket.Conn
	closeCh  chan struct{}
	sendChan chan *Msg
	IP       string
	handler  MsgHandler

	UserID int64
}

func NewWsConn(ws *websocket.Conn, msgHandler MsgHandler) *WsConn {

	wc := &WsConn{
		ws:       ws,
		sendChan: make(chan *Msg, 1024),
		closeCh:  make(chan struct{}),
		IP:       ws.RemoteAddr().String(),
		handler:  msgHandler,
	}

	return wc
}

func (ws *WsConn) SendNow(method any, imsg any) error {
	buf, err := makeMsg(method, imsg)
	if err != nil {
		return err
	}

	ws.ws.SetWriteDeadline(time.Now().Add(7 * time.Second))

	return ws.ws.WriteMessage(websocket.BinaryMessage, buf)
}

func (ws *WsConn) Send(method any, imsg any) error {

	buf, err := makeMsg(method, imsg)
	if err != nil {
		return err
	}

	ws.sendChan <- &Msg{
		Bytes: buf,
		Type:  websocket.BinaryMessage,
	}

	return nil
}

func (ws *WsConn) Write(bytes []byte) (int, error) {
	if err := ws.ws.WriteMessage(websocket.TextMessage, bytes); err != nil {
		return 0, err
	}
	return len(bytes), nil
}

func (ws *WsConn) readPump() {

	defer func() {
		ws.Close()
		close(ws.closeCh)
	}()

	for {
		ws.ws.SetReadDeadline(time.Now().Add(7 * time.Second))
		_, buf, err := ws.ws.ReadMessage()

		if err != nil {
			logs.Error("ws.read", err)
			break
		}

		ws.handler.OnRecv(ws, buf)
	}
}

func (ws *WsConn) writePump() {

	for {
		select {
		case <-ws.closeCh:
			return

		case msg := <-ws.sendChan:

			ws.ws.SetWriteDeadline(time.Now().Add(7 * time.Second))
			err := ws.ws.WriteMessage(msg.Type, msg.Bytes)

			if err != nil {
				logs.Error("ws.send", err.Error())
				return
			}
		}
	}
}

func (ws *WsConn) Close() {
	ws.ws.Close()
}

func (ws *WsConn) Pump() {
	go ws.writePump()
	ws.readPump()
}

func makeMsg(method any, imsg any) ([]byte, error) {
	data := &pb.S2CData{}

	switch id := method.(type) {
	case string:
		data.Method = method.(string)
	case int64:
		data.Callback = id
	default:
		return nil, errors.New("not support")
	}

	switch msg := imsg.(type) {
	case proto.Message:
		buf, err := proto.Marshal(msg)
		if err != nil {
			return nil, err
		}
		data.Body = buf

	case error:
		data.Error = msg.Error()
	}

	return proto.Marshal(data)
}
