///
//  Generated code. Do not modify.
//  source: pb/msg.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
import 'comm.pbjson.dart' as $0;

@$core.Deprecated('Use msgTypeDescriptor instead')
const MsgType$json = const {
  '1': 'MsgType',
  '2': const [
    const {'1': 'TextMsgType', '2': 0},
    const {'1': 'FileMsgType', '2': 1},
    const {'1': 'ImageMsgType', '2': 2},
    const {'1': 'FocusMsgType', '2': 3},
  ],
};

/// Descriptor for `MsgType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List msgTypeDescriptor = $convert.base64Decode('CgdNc2dUeXBlEg8KC1RleHRNc2dUeXBlEAASDwoLRmlsZU1zZ1R5cGUQARIQCgxJbWFnZU1zZ1R5cGUQAhIQCgxGb2N1c01zZ1R5cGUQAw==');
@$core.Deprecated('Use friendDescriptor instead')
const Friend$json = const {
  '1': 'Friend',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 3, '10': 'ID'},
    const {'1': 'Nickname', '3': 2, '4': 1, '5': 9, '10': 'Nickname'},
    const {'1': 'Avatar', '3': 3, '4': 1, '5': 9, '10': 'Avatar'},
    const {'1': 'Online', '3': 4, '4': 1, '5': 8, '10': 'Online'},
    const {'1': 'Group', '3': 5, '4': 1, '5': 8, '10': 'Group'},
  ],
};

/// Descriptor for `Friend`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List friendDescriptor = $convert.base64Decode('CgZGcmllbmQSDgoCSUQYASABKANSAklEEhoKCE5pY2tuYW1lGAIgASgJUghOaWNrbmFtZRIWCgZBdmF0YXIYAyABKAlSBkF2YXRhchIWCgZPbmxpbmUYBCABKAhSBk9ubGluZRIUCgVHcm91cBgFIAEoCFIFR3JvdXA=');
@$core.Deprecated('Use onLoginDescriptor instead')
const OnLogin$json = const {
  '1': 'OnLogin',
  '2': const [
    const {'1': 'Me', '3': 1, '4': 1, '5': 11, '6': '.Friend', '10': 'Me'},
    const {'1': 'Friends', '3': 2, '4': 3, '5': 11, '6': '.Friend', '10': 'Friends'},
    const {'1': 'Msg', '3': 3, '4': 1, '5': 9, '10': 'Msg'},
    const {'1': 'Reconnect', '3': 4, '4': 1, '5': 8, '10': 'Reconnect'},
  ],
};

/// Descriptor for `OnLogin`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onLoginDescriptor = $convert.base64Decode('CgdPbkxvZ2luEhcKAk1lGAEgASgLMgcuRnJpZW5kUgJNZRIhCgdGcmllbmRzGAIgAygLMgcuRnJpZW5kUgdGcmllbmRzEhAKA01zZxgDIAEoCVIDTXNnEhwKCVJlY29ubmVjdBgEIAEoCFIJUmVjb25uZWN0');
@$core.Deprecated('Use kickReqDescriptor instead')
const KickReq$json = const {
  '1': 'KickReq',
  '2': const [
    const {'1': 'Msg', '3': 1, '4': 1, '5': 9, '10': 'Msg'},
  ],
};

/// Descriptor for `KickReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List kickReqDescriptor = $convert.base64Decode('CgdLaWNrUmVxEhAKA01zZxgBIAEoCVIDTXNn');
@$core.Deprecated('Use onlineReqDescriptor instead')
const OnlineReq$json = const {
  '1': 'OnlineReq',
  '2': const [
    const {'1': 'Who', '3': 1, '4': 1, '5': 11, '6': '.Friend', '10': 'Who'},
  ],
};

/// Descriptor for `OnlineReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onlineReqDescriptor = $convert.base64Decode('CglPbmxpbmVSZXESGQoDV2hvGAEgASgLMgcuRnJpZW5kUgNXaG8=');
@$core.Deprecated('Use offlineReqDescriptor instead')
const OfflineReq$json = const {
  '1': 'OfflineReq',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 3, '10': 'ID'},
  ],
};

/// Descriptor for `OfflineReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List offlineReqDescriptor = $convert.base64Decode('CgpPZmZsaW5lUmVxEg4KAklEGAEgASgDUgJJRA==');
@$core.Deprecated('Use statusReqDescriptor instead')
const StatusReq$json = const {
  '1': 'StatusReq',
  '2': const [
    const {'1': 'Who', '3': 1, '4': 1, '5': 11, '6': '.Friend', '10': 'Who'},
  ],
};

/// Descriptor for `StatusReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statusReqDescriptor = $convert.base64Decode('CglTdGF0dXNSZXESGQoDV2hvGAEgASgLMgcuRnJpZW5kUgNXaG8=');
@$core.Deprecated('Use onUploadReqDescriptor instead')
const OnUploadReq$json = const {
  '1': 'OnUploadReq',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'From', '3': 2, '4': 1, '5': 3, '10': 'From'},
  ],
};

/// Descriptor for `OnUploadReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onUploadReqDescriptor = $convert.base64Decode('CgtPblVwbG9hZFJlcRIOCgJJRBgBIAEoCVICSUQSEgoERnJvbRgCIAEoA1IERnJvbQ==');
@$core.Deprecated('Use textMsgDescriptor instead')
const TextMsg$json = const {
  '1': 'TextMsg',
  '2': const [
    const {'1': 'Text', '3': 1, '4': 1, '5': 9, '10': 'Text'},
  ],
};

/// Descriptor for `TextMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List textMsgDescriptor = $convert.base64Decode('CgdUZXh0TXNnEhIKBFRleHQYASABKAlSBFRleHQ=');
@$core.Deprecated('Use imageMsgDescriptor instead')
const ImageMsg$json = const {
  '1': 'ImageMsg',
  '2': const [
    const {'1': 'FileName', '3': 1, '4': 1, '5': 9, '10': 'FileName'},
  ],
};

/// Descriptor for `ImageMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List imageMsgDescriptor = $convert.base64Decode('CghJbWFnZU1zZxIaCghGaWxlTmFtZRgBIAEoCVIIRmlsZU5hbWU=');
@$core.Deprecated('Use fileMsgDescriptor instead')
const FileMsg$json = const {
  '1': 'FileMsg',
  '2': const [
    const {'1': 'FileName', '3': 1, '4': 1, '5': 9, '10': 'FileName'},
  ],
};

/// Descriptor for `FileMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fileMsgDescriptor = $convert.base64Decode('CgdGaWxlTXNnEhoKCEZpbGVOYW1lGAEgASgJUghGaWxlTmFtZQ==');
@$core.Deprecated('Use focusMsgDescriptor instead')
const FocusMsg$json = const {
  '1': 'FocusMsg',
};

/// Descriptor for `FocusMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List focusMsgDescriptor = $convert.base64Decode('CghGb2N1c01zZw==');
@$core.Deprecated('Use msgDescriptor instead')
const Msg$json = const {
  '1': 'Msg',
  '2': const [
    const {'1': 'Sender', '3': 1, '4': 1, '5': 3, '10': 'Sender'},
    const {'1': 'From', '3': 2, '4': 1, '5': 3, '10': 'From'},
    const {'1': 'To', '3': 3, '4': 1, '5': 3, '10': 'To'},
    const {'1': 'ID', '3': 4, '4': 1, '5': 9, '10': 'ID'},
    const {'1': 'Type', '3': 5, '4': 1, '5': 14, '6': '.MsgType', '10': 'Type'},
    const {'1': 'Data', '3': 6, '4': 1, '5': 12, '10': 'Data'},
  ],
};

/// Descriptor for `Msg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List msgDescriptor = $convert.base64Decode('CgNNc2cSFgoGU2VuZGVyGAEgASgDUgZTZW5kZXISEgoERnJvbRgCIAEoA1IERnJvbRIOCgJUbxgDIAEoA1ICVG8SDgoCSUQYBCABKAlSAklEEhwKBFR5cGUYBSABKA4yCC5Nc2dUeXBlUgRUeXBlEhIKBERhdGEYBiABKAxSBERhdGE=');
@$core.Deprecated('Use modifyNameReqDescriptor instead')
const ModifyNameReq$json = const {
  '1': 'ModifyNameReq',
  '2': const [
    const {'1': 'Nickname', '3': 1, '4': 1, '5': 9, '10': 'Nickname'},
    const {'1': 'Avatar', '3': 2, '4': 1, '5': 9, '10': 'Avatar'},
  ],
};

/// Descriptor for `ModifyNameReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List modifyNameReqDescriptor = $convert.base64Decode('Cg1Nb2RpZnlOYW1lUmVxEhoKCE5pY2tuYW1lGAEgASgJUghOaWNrbmFtZRIWCgZBdmF0YXIYAiABKAlSBkF2YXRhcg==');
const $core.Map<$core.String, $core.dynamic> S2CServiceBase$json = const {
  '1': 'S2C',
  '2': const [
    const {'1': 'Login', '2': '.OnLogin', '3': '.Empty'},
    const {'1': 'Kick', '2': '.KickReq', '3': '.Empty'},
    const {'1': 'OnMsg', '2': '.Msg', '3': '.Empty'},
    const {'1': 'Online', '2': '.OnlineReq', '3': '.Empty'},
    const {'1': 'Offline', '2': '.OfflineReq', '3': '.Empty'},
    const {'1': 'Status', '2': '.StatusReq', '3': '.Empty'},
    const {'1': 'OnUpload', '2': '.OnUploadReq', '3': '.Empty'},
  ],
};

@$core.Deprecated('Use s2CServiceDescriptor instead')
const $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> S2CServiceBase$messageJson = const {
  '.OnLogin': OnLogin$json,
  '.Friend': Friend$json,
  '.Empty': $0.Empty$json,
  '.KickReq': KickReq$json,
  '.Msg': Msg$json,
  '.OnlineReq': OnlineReq$json,
  '.OfflineReq': OfflineReq$json,
  '.StatusReq': StatusReq$json,
  '.OnUploadReq': OnUploadReq$json,
};

/// Descriptor for `S2C`. Decode as a `google.protobuf.ServiceDescriptorProto`.
final $typed_data.Uint8List s2CServiceDescriptor = $convert.base64Decode('CgNTMkMSGQoFTG9naW4SCC5PbkxvZ2luGgYuRW1wdHkSGAoES2ljaxIILktpY2tSZXEaBi5FbXB0eRIVCgVPbk1zZxIELk1zZxoGLkVtcHR5EhwKBk9ubGluZRIKLk9ubGluZVJlcRoGLkVtcHR5Eh4KB09mZmxpbmUSCy5PZmZsaW5lUmVxGgYuRW1wdHkSHAoGU3RhdHVzEgouU3RhdHVzUmVxGgYuRW1wdHkSIAoIT25VcGxvYWQSDC5PblVwbG9hZFJlcRoGLkVtcHR5');
const $core.Map<$core.String, $core.dynamic> ChatServiceBase$json = const {
  '1': 'Chat',
  '2': const [
    const {'1': 'Ping', '2': '.Empty', '3': '.Empty'},
    const {'1': 'Send', '2': '.Msg', '3': '.Msg'},
    const {'1': 'ModifyName', '2': '.ModifyNameReq', '3': '.Empty'},
  ],
};

@$core.Deprecated('Use chatServiceDescriptor instead')
const $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> ChatServiceBase$messageJson = const {
  '.Empty': $0.Empty$json,
  '.Msg': Msg$json,
  '.ModifyNameReq': ModifyNameReq$json,
};

/// Descriptor for `Chat`. Decode as a `google.protobuf.ServiceDescriptorProto`.
final $typed_data.Uint8List chatServiceDescriptor = $convert.base64Decode('CgRDaGF0EhYKBFBpbmcSBi5FbXB0eRoGLkVtcHR5EhIKBFNlbmQSBC5Nc2caBC5Nc2cSJAoKTW9kaWZ5TmFtZRIOLk1vZGlmeU5hbWVSZXEaBi5FbXB0eQ==');
