import 'package:desktop_drop/desktop_drop.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:octopus/client.dart';
import 'package:octopus/data.dart';
import 'package:octopus/pb/msg.pb.dart';
import 'package:path/path.dart' as path;

// class PinYinEditCtrl extends TextEditingController {
//   var completeText = '';
//   var finishedEditing = true;

//   @override
//   TextSpan buildTextSpan(
//       {required BuildContext context,
//       TextStyle? style,
//       required bool withComposing}) {
//     if (!value.composing.isValid || !withComposing) {
//       if (completeText != value.text) {
//         completeText = value.text;
//         finishedEditing = true;
//       } else {
//         finishedEditing = false;
//       }
//     }

//     return super.buildTextSpan(context: context, withComposing: withComposing);
//   }
// }

class ChatInput extends StatefulWidget {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  ChatInput({Key? key}) : super(key: key);

  @override
  _ChatInputState createState() => _ChatInputState();
}

class _ChatInputState extends State<ChatInput> {

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    var bgcolor = Colors.white;
    if (theme.brightness == Brightness.dark) {
      bgcolor = Color(0xFF121212);
    }

    return RawKeyboardListener(
      focusNode: FocusNode(),
      onKey: (RawKeyEvent event) async {
        var typ = event.runtimeType.toString();
        if (typ != "RawKeyDownEvent") {
          return;
        }

        Future.delayed(const Duration(milliseconds: 100), () async {
          // if (widget.controller.finishedEditing) {
            if (event.logicalKey == LogicalKeyboardKey.enter &&
                !event.isShiftPressed) {
              var text = widget.controller.text.trim();
              if (text != "") {
                await Client.sendMsg(Data.data.chatTarget.iD,
                    TextMsg(text: text), MsgType.TextMsgType);
                print("完成发送:" + text);
                widget.controller.text = "";
              }
            }
          // }
        });
      },
      child: DropTarget(
        onDragDone: (detail) {
          setState(() {
            var file = detail.files[0];

            var ext = path.extension(file.path).toLowerCase();
            if (ext == '.jpg' ||
                ext == ".jpeg" ||
                ext == ".png" ||
                ext == ".gif" ||
                ext == ".webp") {
              Client.sendFile(file.path, true);
            } else {
              Client.sendFile(file.path, false);
            }
          });
        },
        onDragEntered: (detail) {
          setState(() {
          });
        },
        onDragExited: (detail) {
          setState(() {
          });
        },
        child: TextField(
          controller: widget.controller,
          keyboardType: TextInputType.multiline,
          maxLines: 20,
          focusNode: widget.focusNode,
          decoration: InputDecoration(
            filled: true,
            hoverColor: Colors.transparent,
            contentPadding: EdgeInsets.all(3),
            border: InputBorder.none,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            fillColor: bgcolor,
          ),
        ),
      ),
    );
  }
}
