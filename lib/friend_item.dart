import 'package:flutter/material.dart';
import 'package:octopus/data.dart';
import 'package:badges/badges.dart' as badges;
import 'avatar.dart';

class FriendItem extends StatefulWidget {
  User _user = User();

  FriendItem({Key? key, required User user}) : super(key: key) {
    _user = user;
  }

  @override
  _FriendItem createState() {
    return _FriendItem();
  }
}

class _FriendItem extends State<FriendItem> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return GestureDetector(
      onTap: () {
        Data.data.chatTarget = widget._user;
      },
      child: Container(
        height: 44,
        color: widget._user.iscurrent ? theme.primaryColor : Colors.transparent,
        child: Row(
          children: [
            const SizedBox(width: 4),
            Avatar(
              user: widget._user,
            ),
            const SizedBox(
              width: 6,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                height: 53.5,
                child: Text(
                  widget._user.group ? ("[群]" + widget._user.nickname) :
                  widget._user.nickname,
                  style: const TextStyle(
                    fontSize: 14,
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            badges.Badge(
              badgeContent: Text(widget._user.unread.toString()),
              showBadge: widget._user.unread > 0,
            ),
            const SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
