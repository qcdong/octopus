///
//  Generated code. Do not modify.
//  source: pb/msg.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'comm.pb.dart' as $0;

import 'msg.pbenum.dart';

export 'msg.pbenum.dart';

class Friend extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Friend', createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', protoName: 'ID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Nickname', protoName: 'Nickname')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Avatar', protoName: 'Avatar')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Online', protoName: 'Online')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Group', protoName: 'Group')
    ..hasRequiredFields = false
  ;

  Friend._() : super();
  factory Friend({
    $fixnum.Int64? iD,
    $core.String? nickname,
    $core.String? avatar,
    $core.bool? online,
    $core.bool? group,
  }) {
    final _result = create();
    if (iD != null) {
      _result.iD = iD;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (avatar != null) {
      _result.avatar = avatar;
    }
    if (online != null) {
      _result.online = online;
    }
    if (group != null) {
      _result.group = group;
    }
    return _result;
  }
  factory Friend.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Friend.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Friend clone() => Friend()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Friend copyWith(void Function(Friend) updates) => super.copyWith((message) => updates(message as Friend)) as Friend; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Friend create() => Friend._();
  Friend createEmptyInstance() => create();
  static $pb.PbList<Friend> createRepeated() => $pb.PbList<Friend>();
  @$core.pragma('dart2js:noInline')
  static Friend getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Friend>(create);
  static Friend? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nickname => $_getSZ(1);
  @$pb.TagNumber(2)
  set nickname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNickname() => $_has(1);
  @$pb.TagNumber(2)
  void clearNickname() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get avatar => $_getSZ(2);
  @$pb.TagNumber(3)
  set avatar($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAvatar() => $_has(2);
  @$pb.TagNumber(3)
  void clearAvatar() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get online => $_getBF(3);
  @$pb.TagNumber(4)
  set online($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasOnline() => $_has(3);
  @$pb.TagNumber(4)
  void clearOnline() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get group => $_getBF(4);
  @$pb.TagNumber(5)
  set group($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasGroup() => $_has(4);
  @$pb.TagNumber(5)
  void clearGroup() => clearField(5);
}

class OnLogin extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnLogin', createEmptyInstance: create)
    ..aOM<Friend>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Me', protoName: 'Me', subBuilder: Friend.create)
    ..pc<Friend>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Friends', $pb.PbFieldType.PM, protoName: 'Friends', subBuilder: Friend.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Msg', protoName: 'Msg')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Reconnect', protoName: 'Reconnect')
    ..hasRequiredFields = false
  ;

  OnLogin._() : super();
  factory OnLogin({
    Friend? me,
    $core.Iterable<Friend>? friends,
    $core.String? msg,
    $core.bool? reconnect,
  }) {
    final _result = create();
    if (me != null) {
      _result.me = me;
    }
    if (friends != null) {
      _result.friends.addAll(friends);
    }
    if (msg != null) {
      _result.msg = msg;
    }
    if (reconnect != null) {
      _result.reconnect = reconnect;
    }
    return _result;
  }
  factory OnLogin.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnLogin.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnLogin clone() => OnLogin()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnLogin copyWith(void Function(OnLogin) updates) => super.copyWith((message) => updates(message as OnLogin)) as OnLogin; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnLogin create() => OnLogin._();
  OnLogin createEmptyInstance() => create();
  static $pb.PbList<OnLogin> createRepeated() => $pb.PbList<OnLogin>();
  @$core.pragma('dart2js:noInline')
  static OnLogin getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnLogin>(create);
  static OnLogin? _defaultInstance;

  @$pb.TagNumber(1)
  Friend get me => $_getN(0);
  @$pb.TagNumber(1)
  set me(Friend v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMe() => $_has(0);
  @$pb.TagNumber(1)
  void clearMe() => clearField(1);
  @$pb.TagNumber(1)
  Friend ensureMe() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Friend> get friends => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get msg => $_getSZ(2);
  @$pb.TagNumber(3)
  set msg($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMsg() => $_has(2);
  @$pb.TagNumber(3)
  void clearMsg() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get reconnect => $_getBF(3);
  @$pb.TagNumber(4)
  set reconnect($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasReconnect() => $_has(3);
  @$pb.TagNumber(4)
  void clearReconnect() => clearField(4);
}

class KickReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'KickReq', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Msg', protoName: 'Msg')
    ..hasRequiredFields = false
  ;

  KickReq._() : super();
  factory KickReq({
    $core.String? msg,
  }) {
    final _result = create();
    if (msg != null) {
      _result.msg = msg;
    }
    return _result;
  }
  factory KickReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory KickReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  KickReq clone() => KickReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  KickReq copyWith(void Function(KickReq) updates) => super.copyWith((message) => updates(message as KickReq)) as KickReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static KickReq create() => KickReq._();
  KickReq createEmptyInstance() => create();
  static $pb.PbList<KickReq> createRepeated() => $pb.PbList<KickReq>();
  @$core.pragma('dart2js:noInline')
  static KickReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<KickReq>(create);
  static KickReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get msg => $_getSZ(0);
  @$pb.TagNumber(1)
  set msg($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMsg() => $_has(0);
  @$pb.TagNumber(1)
  void clearMsg() => clearField(1);
}

class OnlineReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnlineReq', createEmptyInstance: create)
    ..aOM<Friend>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Who', protoName: 'Who', subBuilder: Friend.create)
    ..hasRequiredFields = false
  ;

  OnlineReq._() : super();
  factory OnlineReq({
    Friend? who,
  }) {
    final _result = create();
    if (who != null) {
      _result.who = who;
    }
    return _result;
  }
  factory OnlineReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnlineReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnlineReq clone() => OnlineReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnlineReq copyWith(void Function(OnlineReq) updates) => super.copyWith((message) => updates(message as OnlineReq)) as OnlineReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnlineReq create() => OnlineReq._();
  OnlineReq createEmptyInstance() => create();
  static $pb.PbList<OnlineReq> createRepeated() => $pb.PbList<OnlineReq>();
  @$core.pragma('dart2js:noInline')
  static OnlineReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnlineReq>(create);
  static OnlineReq? _defaultInstance;

  @$pb.TagNumber(1)
  Friend get who => $_getN(0);
  @$pb.TagNumber(1)
  set who(Friend v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasWho() => $_has(0);
  @$pb.TagNumber(1)
  void clearWho() => clearField(1);
  @$pb.TagNumber(1)
  Friend ensureWho() => $_ensure(0);
}

class OfflineReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineReq', createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', protoName: 'ID')
    ..hasRequiredFields = false
  ;

  OfflineReq._() : super();
  factory OfflineReq({
    $fixnum.Int64? iD,
  }) {
    final _result = create();
    if (iD != null) {
      _result.iD = iD;
    }
    return _result;
  }
  factory OfflineReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineReq clone() => OfflineReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineReq copyWith(void Function(OfflineReq) updates) => super.copyWith((message) => updates(message as OfflineReq)) as OfflineReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineReq create() => OfflineReq._();
  OfflineReq createEmptyInstance() => create();
  static $pb.PbList<OfflineReq> createRepeated() => $pb.PbList<OfflineReq>();
  @$core.pragma('dart2js:noInline')
  static OfflineReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineReq>(create);
  static OfflineReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class StatusReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StatusReq', createEmptyInstance: create)
    ..aOM<Friend>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Who', protoName: 'Who', subBuilder: Friend.create)
    ..hasRequiredFields = false
  ;

  StatusReq._() : super();
  factory StatusReq({
    Friend? who,
  }) {
    final _result = create();
    if (who != null) {
      _result.who = who;
    }
    return _result;
  }
  factory StatusReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StatusReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StatusReq clone() => StatusReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StatusReq copyWith(void Function(StatusReq) updates) => super.copyWith((message) => updates(message as StatusReq)) as StatusReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusReq create() => StatusReq._();
  StatusReq createEmptyInstance() => create();
  static $pb.PbList<StatusReq> createRepeated() => $pb.PbList<StatusReq>();
  @$core.pragma('dart2js:noInline')
  static StatusReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StatusReq>(create);
  static StatusReq? _defaultInstance;

  @$pb.TagNumber(1)
  Friend get who => $_getN(0);
  @$pb.TagNumber(1)
  set who(Friend v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasWho() => $_has(0);
  @$pb.TagNumber(1)
  void clearWho() => clearField(1);
  @$pb.TagNumber(1)
  Friend ensureWho() => $_ensure(0);
}

class OnUploadReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnUploadReq', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', protoName: 'ID')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'From', protoName: 'From')
    ..hasRequiredFields = false
  ;

  OnUploadReq._() : super();
  factory OnUploadReq({
    $core.String? iD,
    $fixnum.Int64? from,
  }) {
    final _result = create();
    if (iD != null) {
      _result.iD = iD;
    }
    if (from != null) {
      _result.from = from;
    }
    return _result;
  }
  factory OnUploadReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnUploadReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnUploadReq clone() => OnUploadReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnUploadReq copyWith(void Function(OnUploadReq) updates) => super.copyWith((message) => updates(message as OnUploadReq)) as OnUploadReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnUploadReq create() => OnUploadReq._();
  OnUploadReq createEmptyInstance() => create();
  static $pb.PbList<OnUploadReq> createRepeated() => $pb.PbList<OnUploadReq>();
  @$core.pragma('dart2js:noInline')
  static OnUploadReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnUploadReq>(create);
  static OnUploadReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get iD => $_getSZ(0);
  @$pb.TagNumber(1)
  set iD($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get from => $_getI64(1);
  @$pb.TagNumber(2)
  set from($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFrom() => $_has(1);
  @$pb.TagNumber(2)
  void clearFrom() => clearField(2);
}

class TextMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TextMsg', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Text', protoName: 'Text')
    ..hasRequiredFields = false
  ;

  TextMsg._() : super();
  factory TextMsg({
    $core.String? text,
  }) {
    final _result = create();
    if (text != null) {
      _result.text = text;
    }
    return _result;
  }
  factory TextMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TextMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TextMsg clone() => TextMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TextMsg copyWith(void Function(TextMsg) updates) => super.copyWith((message) => updates(message as TextMsg)) as TextMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TextMsg create() => TextMsg._();
  TextMsg createEmptyInstance() => create();
  static $pb.PbList<TextMsg> createRepeated() => $pb.PbList<TextMsg>();
  @$core.pragma('dart2js:noInline')
  static TextMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TextMsg>(create);
  static TextMsg? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get text => $_getSZ(0);
  @$pb.TagNumber(1)
  set text($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasText() => $_has(0);
  @$pb.TagNumber(1)
  void clearText() => clearField(1);
}

class ImageMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ImageMsg', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'FileName', protoName: 'FileName')
    ..hasRequiredFields = false
  ;

  ImageMsg._() : super();
  factory ImageMsg({
    $core.String? fileName,
  }) {
    final _result = create();
    if (fileName != null) {
      _result.fileName = fileName;
    }
    return _result;
  }
  factory ImageMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ImageMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ImageMsg clone() => ImageMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ImageMsg copyWith(void Function(ImageMsg) updates) => super.copyWith((message) => updates(message as ImageMsg)) as ImageMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ImageMsg create() => ImageMsg._();
  ImageMsg createEmptyInstance() => create();
  static $pb.PbList<ImageMsg> createRepeated() => $pb.PbList<ImageMsg>();
  @$core.pragma('dart2js:noInline')
  static ImageMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ImageMsg>(create);
  static ImageMsg? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fileName => $_getSZ(0);
  @$pb.TagNumber(1)
  set fileName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFileName() => $_has(0);
  @$pb.TagNumber(1)
  void clearFileName() => clearField(1);
}

class FileMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FileMsg', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'FileName', protoName: 'FileName')
    ..hasRequiredFields = false
  ;

  FileMsg._() : super();
  factory FileMsg({
    $core.String? fileName,
  }) {
    final _result = create();
    if (fileName != null) {
      _result.fileName = fileName;
    }
    return _result;
  }
  factory FileMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FileMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FileMsg clone() => FileMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FileMsg copyWith(void Function(FileMsg) updates) => super.copyWith((message) => updates(message as FileMsg)) as FileMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FileMsg create() => FileMsg._();
  FileMsg createEmptyInstance() => create();
  static $pb.PbList<FileMsg> createRepeated() => $pb.PbList<FileMsg>();
  @$core.pragma('dart2js:noInline')
  static FileMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FileMsg>(create);
  static FileMsg? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fileName => $_getSZ(0);
  @$pb.TagNumber(1)
  set fileName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFileName() => $_has(0);
  @$pb.TagNumber(1)
  void clearFileName() => clearField(1);
}

class FocusMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FocusMsg', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  FocusMsg._() : super();
  factory FocusMsg() => create();
  factory FocusMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FocusMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FocusMsg clone() => FocusMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FocusMsg copyWith(void Function(FocusMsg) updates) => super.copyWith((message) => updates(message as FocusMsg)) as FocusMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FocusMsg create() => FocusMsg._();
  FocusMsg createEmptyInstance() => create();
  static $pb.PbList<FocusMsg> createRepeated() => $pb.PbList<FocusMsg>();
  @$core.pragma('dart2js:noInline')
  static FocusMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FocusMsg>(create);
  static FocusMsg? _defaultInstance;
}

class Msg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Msg', createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Sender', protoName: 'Sender')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'From', protoName: 'From')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'To', protoName: 'To')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', protoName: 'ID')
    ..e<MsgType>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Type', $pb.PbFieldType.OE, protoName: 'Type', defaultOrMaker: MsgType.TextMsgType, valueOf: MsgType.valueOf, enumValues: MsgType.values)
    ..a<$core.List<$core.int>>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Data', $pb.PbFieldType.OY, protoName: 'Data')
    ..hasRequiredFields = false
  ;

  Msg._() : super();
  factory Msg({
    $fixnum.Int64? sender,
    $fixnum.Int64? from,
    $fixnum.Int64? to,
    $core.String? iD,
    MsgType? type,
    $core.List<$core.int>? data,
  }) {
    final _result = create();
    if (sender != null) {
      _result.sender = sender;
    }
    if (from != null) {
      _result.from = from;
    }
    if (to != null) {
      _result.to = to;
    }
    if (iD != null) {
      _result.iD = iD;
    }
    if (type != null) {
      _result.type = type;
    }
    if (data != null) {
      _result.data = data;
    }
    return _result;
  }
  factory Msg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Msg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Msg clone() => Msg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Msg copyWith(void Function(Msg) updates) => super.copyWith((message) => updates(message as Msg)) as Msg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Msg create() => Msg._();
  Msg createEmptyInstance() => create();
  static $pb.PbList<Msg> createRepeated() => $pb.PbList<Msg>();
  @$core.pragma('dart2js:noInline')
  static Msg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Msg>(create);
  static Msg? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get sender => $_getI64(0);
  @$pb.TagNumber(1)
  set sender($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSender() => $_has(0);
  @$pb.TagNumber(1)
  void clearSender() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get from => $_getI64(1);
  @$pb.TagNumber(2)
  set from($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFrom() => $_has(1);
  @$pb.TagNumber(2)
  void clearFrom() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get to => $_getI64(2);
  @$pb.TagNumber(3)
  set to($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTo() => $_has(2);
  @$pb.TagNumber(3)
  void clearTo() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get iD => $_getSZ(3);
  @$pb.TagNumber(4)
  set iD($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasID() => $_has(3);
  @$pb.TagNumber(4)
  void clearID() => clearField(4);

  @$pb.TagNumber(5)
  MsgType get type => $_getN(4);
  @$pb.TagNumber(5)
  set type(MsgType v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasType() => $_has(4);
  @$pb.TagNumber(5)
  void clearType() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<$core.int> get data => $_getN(5);
  @$pb.TagNumber(6)
  set data($core.List<$core.int> v) { $_setBytes(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasData() => $_has(5);
  @$pb.TagNumber(6)
  void clearData() => clearField(6);
}

class ModifyNameReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ModifyNameReq', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Nickname', protoName: 'Nickname')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Avatar', protoName: 'Avatar')
    ..hasRequiredFields = false
  ;

  ModifyNameReq._() : super();
  factory ModifyNameReq({
    $core.String? nickname,
    $core.String? avatar,
  }) {
    final _result = create();
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (avatar != null) {
      _result.avatar = avatar;
    }
    return _result;
  }
  factory ModifyNameReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ModifyNameReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ModifyNameReq clone() => ModifyNameReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ModifyNameReq copyWith(void Function(ModifyNameReq) updates) => super.copyWith((message) => updates(message as ModifyNameReq)) as ModifyNameReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ModifyNameReq create() => ModifyNameReq._();
  ModifyNameReq createEmptyInstance() => create();
  static $pb.PbList<ModifyNameReq> createRepeated() => $pb.PbList<ModifyNameReq>();
  @$core.pragma('dart2js:noInline')
  static ModifyNameReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ModifyNameReq>(create);
  static ModifyNameReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get nickname => $_getSZ(0);
  @$pb.TagNumber(1)
  set nickname($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNickname() => $_has(0);
  @$pb.TagNumber(1)
  void clearNickname() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get avatar => $_getSZ(1);
  @$pb.TagNumber(2)
  set avatar($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAvatar() => $_has(1);
  @$pb.TagNumber(2)
  void clearAvatar() => clearField(2);
}

class S2CApi {
  $pb.RpcClient _client;
  S2CApi(this._client);

  $async.Future<$0.Empty> login($pb.ClientContext? ctx, OnLogin request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'Login', request, emptyResponse);
  }
  $async.Future<$0.Empty> kick($pb.ClientContext? ctx, KickReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'Kick', request, emptyResponse);
  }
  $async.Future<$0.Empty> onMsg($pb.ClientContext? ctx, Msg request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'OnMsg', request, emptyResponse);
  }
  $async.Future<$0.Empty> online($pb.ClientContext? ctx, OnlineReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'Online', request, emptyResponse);
  }
  $async.Future<$0.Empty> offline($pb.ClientContext? ctx, OfflineReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'Offline', request, emptyResponse);
  }
  $async.Future<$0.Empty> status($pb.ClientContext? ctx, StatusReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'Status', request, emptyResponse);
  }
  $async.Future<$0.Empty> onUpload($pb.ClientContext? ctx, OnUploadReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'S2C', 'OnUpload', request, emptyResponse);
  }
}

class ChatApi {
  $pb.RpcClient _client;
  ChatApi(this._client);

  $async.Future<$0.Empty> ping($pb.ClientContext? ctx, $0.Empty request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'Chat', 'Ping', request, emptyResponse);
  }
  $async.Future<Msg> send($pb.ClientContext? ctx, Msg request) {
    var emptyResponse = Msg();
    return _client.invoke<Msg>(ctx, 'Chat', 'Send', request, emptyResponse);
  }
  $async.Future<$0.Empty> modifyName($pb.ClientContext? ctx, ModifyNameReq request) {
    var emptyResponse = $0.Empty();
    return _client.invoke<$0.Empty>(ctx, 'Chat', 'ModifyName', request, emptyResponse);
  }
}

