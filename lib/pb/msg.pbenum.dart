///
//  Generated code. Do not modify.
//  source: pb/msg.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class MsgType extends $pb.ProtobufEnum {
  static const MsgType TextMsgType = MsgType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TextMsgType');
  static const MsgType FileMsgType = MsgType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FileMsgType');
  static const MsgType ImageMsgType = MsgType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ImageMsgType');
  static const MsgType FocusMsgType = MsgType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FocusMsgType');

  static const $core.List<MsgType> values = <MsgType> [
    TextMsgType,
    FileMsgType,
    ImageMsgType,
    FocusMsgType,
  ];

  static final $core.Map<$core.int, MsgType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static MsgType? valueOf($core.int value) => _byValue[value];

  const MsgType._($core.int v, $core.String n) : super(v, n);
}

